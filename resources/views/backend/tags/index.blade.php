<x-backend.layouts.master>
    <h1 class="mt-4">Tags</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">tags</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            tag List

            <!-- <a class="btn btn-sm btn-info" href="{{ route('tags.pdf') }}">PDF</a>
            @can('tag-trash-list')
            <a class="btn btn-sm btn-info" href="{{ route('tags.trash') }}">Trash List</a>
            @endcan -->
            <a class="btn btn-sm btn-primary" href="{{ route('tags.create') }}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Title</th>
                        
                    </tr>
                </thead>

                <tbody>

                    @foreach ($tags as $tag)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $tag->title }}</td>
                        
                    
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>