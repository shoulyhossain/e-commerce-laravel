<x-backend.layouts.master>
    <h1 class="mt-4">Tags</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Tags</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Tag Details
            <a class="btn btn-sm btn-primary" href="{{ route('tags.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>tag: {{ $tag->tag->title ?? 'N/A' }}</h3>
            <h3>Title: {{ $tag->title }}</h3>
            <p>Created At: {{ $tag->created_at->diffForHumans() }} By: {{$tag->createdBy->name??'N/A'}}</p>
            <p>Updated At: {{ $tag->updated_at->diffForHumans() }} By: {{$tag->updatedBy->name??'N/A'}}</p>
        </div>

    </div>
</x-backend.layouts.master>