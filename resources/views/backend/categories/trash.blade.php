<x-backend.layouts.master>
    <h1 class="mt-4">Categories</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Deleted categories</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Deleted Category List
            <a class="btn btn-sm btn-primary" href="{{ route('categories.index') }}">List</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->price }} TK</td>
                        <td>

                            <a class="btn btn-warning btn-sm" href="{{ route('categories.restore', ['id' => $category->id]) }}">Restore</a>

                            <form action="{{ route('categories.delete', ['id' => $category->id]) }}" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>