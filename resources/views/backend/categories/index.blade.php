<x-backend.layouts.master>
    <h1 class="mt-4">Categories</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">categories</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Category List

            <!-- <a class="btn btn-sm btn-info" href="{{ route('categories.pdf') }}">PDF</a>
            @can('category-trash-list')
            <a class="btn btn-sm btn-info" href="{{ route('categories.trash') }}">Trash List</a>
            @endcan -->
            <a class="btn btn-sm btn-primary" href="{{ route('categories.create') }}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->title }}</td>
                        
                        <td>{{ $category->category->description ?? 'N/A' }}</td>
                        <td>

                            @can('category-show', $category)
                            <a class="btn btn-info btn-sm" href="{{ route('categories.show', ['category' => $category->id]) }}">Show</a>    
                            @endcan

                            @can('category-edit', $category)
                            <a class="btn btn-warning btn-sm" href="{{ route('categories.edit', ['category' => $category->id]) }}">Edit</a>    
                            @endcan
                            
                            @can('category-delete', $category)
                            <form action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>
                            @endcan

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>