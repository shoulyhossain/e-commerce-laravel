<x-backend.layouts.master>
    <h1 class="mt-4">categories</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Categories</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Category Details
            <a class="btn btn-sm btn-primary" href="{{ route('categories.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>Category: {{ $category->category->title ?? 'N/A' }}</h3>
            <h3>Title: {{ $category->title }}</h3>
            <p>Description: {{ $category->description }}</p>
            <p>Price: {{ $category->price }}</p>
            <img src="{{ asset('storage/categories/'. $category->image) }}" />
            <p>Created At: {{ $category->created_at->diffForHumans() }} By: {{$category->createdBy->name??'N/A'}}</p>
            <p>Updated At: {{ $category->updated_at->diffForHumans() }} By: {{$category->updatedBy->name??'N/A'}}</p>
        </div>

    </div>
</x-backend.layouts.master>