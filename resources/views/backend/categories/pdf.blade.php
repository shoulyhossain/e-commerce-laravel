<table border="1" style="width: 700px;">
    <tr>
        <th>Sl#</th>
        <th>Name</th>
        <th>Price</th>
    </tr>
    @foreach ($categories as $category)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $category->title }}</td>
        <td>{{ $category->price }} BDT</td>
    </tr>
    @endforeach
</table>