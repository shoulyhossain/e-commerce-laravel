<x-backend.layouts.master>
    <h1 class="mt-4">Categories</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">categories</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Category Create
            <a class="btn btn-sm btn-primary" href="{{ route('categories.index') }}">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                   
                    @error('category_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="title" class="form-label">Title</label>
                    <input name="title" type="text" class="form-control" id="title" value="{{ old('title') }}">

                    @error('title')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

               

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea name="description" class="form-control" rows="10" id="description">
                        {{ old('description') }}
                    </textarea>
                    @error('description')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                

                @foreach ($tags as $key=>$tag)
                    <input id="{{$key}}" type="checkbox" name="tag_ids[]" value="{{$key}}"/>
                    <label for="{{$key}}">{{ $tag }}</label><br>
                @endforeach

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</x-backend.layouts.master>