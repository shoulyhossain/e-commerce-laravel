<x-frontend.layouts.master>


    <div class="bg-light p-5 rounded">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
            <form method="post" action="{{route('orders.create')}}">
                    @csrf
                <table class="table">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th  class="text-sm"> <span class="text-sm-start"> Unit Price</span></th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $itemTotalPrice = 0;
                        $totalPrice = 0;
                        @endphp
                        @foreach ($cartItems as $cart)


                        @php
                        $itemTotalPrice = $cart->qty * $cart->unit_price;
                        $totalPrice += $itemTotalPrice;
                        @endphp

                        <tr>
                            <td>
                                {{ $loop->iteration }}
                                <input type="hidden" name="product_ids[]" value="{{$cart->product->id}}" />
                            </td>
                            <td>{{ $cart->product->title }}</td>
                            <td>
                                <input type="number" onchange="updatePrice(this)" name="product_qty[]" value="{{$cart->qty}}" />
                            </td>
                            <td><span>{{ $cart->unit_price }}</span></td>
                            <td class="grand_total">{{ $itemTotalPrice }}</td>
                            <td><button type="button" class="btn btn-danger btn-sm">Remove</button></td>
                        </tr>
                        @endforeach

                        
                        <tr>
                            <td align="right" colspan="4">Total Price</td>
                            <td > <span id="total">{{ $totalPrice }}</span></td>
                            <td></td>

                        </tr>
                    </tbody>
                </table>

             
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" >
                    </div>
                    <div class="mb-3">
                        <label for="shipping_address" class="form-label">Address</label>
                        <textarea name="shipping_address" id="shipping_address" class="form-control"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Place Order</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        // const th = document.querySelectorAll('td');
        // for (let i=0 ; i< th.length; i++)
        // {

        // }
        function updatePrice(element){
            const qty = element.value;
            const unitPrice = element.parentElement.nextElementSibling.innerText;
            //updating item total price
            element.parentElement.nextElementSibling.nextElementSibling.innerText = qty * unitPrice ;
            total();
            
        }
        //upadting cart total price
            
        function total()
        {
            const grandTotals = document.querySelectorAll('.grand_total');
            console.log(grandTotals.innerText);
        
            let sum=0;

            grandTotals.forEach(function(grandTotalElement)
            {
                console.log(grandTotalElement.innerText);
                sum+= parseFloat(grandTotalElement.innerText);
            });
            document.querySelector('#total').innerText = sum;
            
        }
        total();
    </script>
</x-frontend.layouts.master>