<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\User;
use App\Policies\ProductPolicy;
use App\Policies\CategoryPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::define('product-edit', function (User $user, Product $product) {
        //     return $user->id === $product->created_by;
        // });

        Gate::define('product-edit', [ProductPolicy::class, 'update']);
        Gate::define('product-show', [ProductPolicy::class, 'view']);
        Gate::define('product-delete', [ProductPolicy::class, 'delete']);
        Gate::define('product-trash-list', [ProductPolicy::class, 'trashList']);


        Gate::define('category-edit', [CategoryPolicy::class, 'update']);
        Gate::define('category-show', [CategoryPolicy::class, 'view']);
        Gate::define('category-delete', [CategoryPolicy::class, 'delete']);
        Gate::define('category-trash-list', [CategoryPolicy::class, 'trashList']);

    }
}
