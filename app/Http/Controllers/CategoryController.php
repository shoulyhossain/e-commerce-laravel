<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Tag;
use GuzzleHttp\Psr7\Query;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Image;
use PDF;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categories = Category::all();
        $categories = Category::orderBy('id', 'desc')->get();
        return view('backend.categories.index', compact('categories'));
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $tags = Tag::pluck('title', 'id')->toArray();
        return view('backend.categories.create', compact('categories', 'tags'));
    }

    public function store(CategoryRequest $request)
    {
        try {
            $requestData = $request->all();

            // $requestData['created_by'] = auth()->user()->id;
            $category = Category::create($requestData);
          
            // Session::flush('message', 'Successfully Saved');
            // return redirect()->route('categories.index')->with('message','Successfully Saved !');
            return redirect()->route('categories.index')->withMessage('Successfully Saved !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Category $category) //dependency injection
    {
        $this->authorize('category-show', $category);
        // $category = Category::where('id', $id)->first();
        // $category = Category::where('id', $id)->firstOrFail();

        // $category = Category::findOrFail($category);

        // return view('backend.categories.show', [
        //     'category' => $category
        // ]);

        return view('backend.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        $this->authorize('category-edit', $category);

        $categories = Category::pluck('title', 'id')->toArray();
        // $category = Category::findOrFail($id);
        return view('backend.categories.edit', compact('category', 'categories'));
    }

    public function update(Request $request, Category $category)
    {
        try {
            $requestData = $request->all();

            // $category = Category::findOrFail($id);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path() . '/app/public/categories/' . $fileName);
                $requestData['image'] = $fileName;
            } else {
                $requestData['image'] = $category->image;
            }

            $requestData['updated_by'] = auth()->id();
            $category->update($requestData);

            return redirect()->route('categories.index')->withMessage('Successfully Updated !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Category $category)
    {
        $this->authorize('category-delete', $category);
        // $category = Category::findOrFail($id);
        $category->update(['deleted_by'=>auth()->id()]);

        $category->delete();

        return redirect()->route('categories.index')->withMessage('Successfully Deleted !');
    }

    public function trash()
    {
        $this->authorize('category-trash-list');

        $categories = Category::onlyTrashed()->get();
        return view('backend.categories.trash', compact('categories'));
    }

    public function restore($id)
    {
        Category::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('categories.trash')->withMessage('Successfully Restored !');
    }

    public function delete($id)
    {
        Category::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('categories.trash')->withMessage('Deleted Successfully');
    }

    public function pdf()
    {
        $categories = Category::latest()->get();
        $pdf = PDF::loadView('backend.categories.pdf', compact('categories'));
        return $pdf->download('categories.pdf');
    }
}
